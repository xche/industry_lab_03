package ictgradschool.industry.lab03.ex02;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */

    private int randomNum(int min, int max) {
        int ranNum = min + (int) (Math.random() * ((max - min) + 1));
        return ranNum;
    }

    private void start() {
        System.out.print("Lower bound?");
        String minS = Keyboard.readInput();
        int min = Integer.parseInt(minS);

        System.out.print("Upper bound?");
        String maxS = Keyboard.readInput();
        int max = Integer.parseInt(maxS);

        int ranNum1 = randomNum(min, max);
        int ranNum2 = randomNum(min, max);
        int ranNum3 = randomNum(min, max);



        System.out.println("3 randomly generated numbers: " + ranNum1 + ", " + ranNum2 + " and " + ranNum3);

        int smallNum = Math.min(ranNum1, Math.min(ranNum2, ranNum3));
        System.out.println("Smallest number is " + smallNum);

    }


    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
